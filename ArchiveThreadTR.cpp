//+=============================================================================
//
// file :         ArchiveThread.cpp
//
// description :  Include for the ArchiveThread class.
//                This class is used for BLMAll archiving thread
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace BLMAll_ns
{
class ArchiveThreadTR;
}

#include <ArchiveThreadTR.h>

static double NaN = sqrt(-1);

namespace BLMAll_ns
{


// ----------------------------------------------------------------------------------------
// Inner class for Tango event callback
// ----------------------------------------------------------------------------------------
class TRCallBacks: public Tango::CallBack {

public:
    TRCallBacks() {}

    void Init(int blmId,ArchiveThreadTR *src) {
      id = blmId;
      source = src;
    }

    void push_event(Tango::EventData *event) {
      source->event(id,event);
    }

    int id;
    ArchiveThreadTR *source;

};

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
ArchiveThreadTR::ArchiveThreadTR(BLMAll *blmall, omni_mutex &m):
        Tango::LogAdapter(blmall), mutex(m), ds(blmall)
{

  INFO_STREAM << "ArchiveThreadTR::ArchiveThreadTR(): entering." << endl;

  tickStart = -1;
  for(int i=0;i<MAX_BLM;i++) {
    subscribed[i] = false;
    lastErrors[i] = true;
    lastEvent[i] = 0;
  }
  exitThread = false;
  frameStart = 0;
  length = 0;
  lastError = string("");
  status = string("");
  nbComplete = 0;
  nbIncomplete = 0;

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ArchiveThreadTR::run_undetached(void *arg) {

  TRCallBacks callBacks[MAX_BLM];
  for(int i=0;i<MAX_BLM;i++)
    callBacks[i].Init(i,this);

  while (!exitThread) {

    time_t t0 = get_ticks();

    string errStr;
    {
      omni_mutex_lock lock(mutex);
      status = string("ArchiveThreadTR: Got frames (complete/incomplete): ") + std::to_string(nbComplete) + "/" + std::to_string(nbIncomplete);
      if(lastError.length()>0)
        status += "\n"+string("ArchiveThreadTR: ") + lastError;
    }

    // Subscribe to all BLMs
    for(int i=0;i<ds->nbBLM;i++) {
      if (!subscribed[i]) {
        try {
          const vector<string> filters;
          Tango::DeviceProxy *dp = ds->deviceGroup->get_device(i+1); // get device starts at one !!!!!
          if( dp!=NULL ) {
            int subscriber_id = dp->subscribe_event("AvgSumRow", Tango::CHANGE_EVENT, &callBacks[i], filters);
            subscribed[i] = true;
          }
        } catch (Tango::DevFailed &e) {
          //cout << "Warning,  " << ds->blmGroup->get_device(i)->dev_name() << "/AvgSum " << e.errors[0].desc << endl;
        }
      }
    }

    // Check frame
    time_t now = get_ticks();
    if( frameStart!=0 && (now-frameStart) > MAX_FRAME_TIME ) {

      if(!checkFrame()) {

        // Imcomplete frame
        // cout << "Imcomplete frame !" << endl;
        int nbError = 0;

        for(int i=0;i<ds->nbBLM;i++) {
          if(!ds->isDisabled(i) && subscribed[i] && lastEvent[i]==0) {
            cout << "TR: BLM C" << (i+1) << " : no data " << endl;
            nbError++;
          }
        }

        if(nbError<5) {
          // Store if less than 5 errors
          nbIncomplete++;
          pushFrame();
        }
        resetFrame();

      }

    }


    time_t t1 = get_ticks();


    time_t toSleep = (int) (100 - (t1 - t0));
    if (toSleep > 0) usleep((unsigned) (toSleep * 1000));

  }

  cout << "ArchiveThreadTR exiting..." << endl;

}

//----------------------------------------------------------------------------
// Check if a frame is complete
//----------------------------------------------------------------------------
bool ArchiveThreadTR::checkFrame() {

  // Check that all events have been sent
  bool ok = true;
  int i = 0;
  while (i < ds->nbBLM && ok) {
    // Ignore when not subscribed or disabled
    ok = ds->isDisabled(i) || !subscribed[i] || lastEvent[i] != 0;
    i++;
  }

  return ok;

}

//----------------------------------------------------------------------------
// Push frame to HDB++
//----------------------------------------------------------------------------
void ArchiveThreadTR::pushFrame() {

  {
    omni_mutex_lock lock(mutex);
    for(int i=0;i<(int)length;i++)
      ds->timeResolvedInjLoss[i] = frame[i];
    ds->timeResolvedInjLossLength = length;
  }

  /*
  try {

    double *frameCopy = new double[BLM_NB*4];
    memcpy(frameCopy,frame,BLM_NB*4*sizeof(double));
    ds->push_archive_event("InjLosses", frameCopy, BLM_NB * 4, 0, true);

    // Total injection loss
    double *sum = new double[1];
    sum[0] = 0;
    for(int i=0;i<BLM_NB*4;i++)
      if(!std::isnan(frameCopy[i])) sum[0] += frame[i];
    ds->attr_TotalInjectionLoss_read[0] = sum[0];
    ds->push_archive_event("TotalInjectionLoss",sum,1,0,true);

  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    ds->archiveStatus = string("Failed to push data to HDB :") + e.errors[0].desc.in();
  }
  */

}

//----------------------------------------------------------------------------
// Reset frame
//----------------------------------------------------------------------------
void ArchiveThreadTR::resetFrame() {

  frameStart = 0;
  for (int i = 0; i < ds->nbBLM; i++) {
    lastEvent[i] = 0;
  }
  for(int i=0; i < MAX_TR_BUFF_LENGTH; i++) {
    frame[i] = 0.0;
  }
  length = 0;

}

//----------------------------------------------------------------------------
// Event callback
//----------------------------------------------------------------------------
void ArchiveThreadTR::event(int blmId, Tango::EventData *event) {

  // Serialise call
  omni_mutex_lock lock(eventMutex);

  // Ignore subscription event and event coming from synchronous reading
  if(lastErrors[blmId]) {
    lastErrors[blmId]=0;
    return;
  }

  time_t now = get_ticks();

  if (!event->err) {

    try {

      vector<Tango::DevLong> data;
      *(event->attr_value) >> data;
      if(length==0) {

        // First event, get the length
        length = data.size();
        if(length > MAX_TR_BUFF_LENGTH) length = MAX_TR_BUFF_LENGTH;

      } else {

        if(data.size()!=length) {
          // Buffer size changed during frame
          resetFrame();
          return;
        }

      }

      if (frameStart==0) {
        // We start a new frame
        // cout << "Starting new frame " << endl;
        resetFrame();
        frameStart = now;
      }

      for(int i=0;i<(int)length;i++)
        frame[i] += (double)data[i];
      lastEvent[blmId] = now;

      if (checkFrame()) {
        // Complete frame
        // cout << "Complete frame:" << (now - frameStart) << " ms" << endl;
        nbComplete++;
        pushFrame();
        resetFrame();
      }

    } catch (Tango::DevFailed &e) {

      cout << "ArchiveThreadTR::event() got error for BLM#" << blmId << " " << e.errors[0].desc << endl;

    }

  } else {

    lastErrors[blmId] = true;
    cout << "ArchiveThreadTR::event() got error for BLM#" << blmId << " " << event->errors[0].desc << endl;

  }


}

//----------------------------------------------------------------------------
// Return number of milliseconds
//-----------------------------------------------------------------------------
time_t ArchiveThreadTR::get_ticks() {

  if(tickStart < 0 )
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}

} // namespace BLMAll_ns
