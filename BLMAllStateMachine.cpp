/*----- PROTECTED REGION ID(BLMAllStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        BLMAllStateMachine.cpp
//
// description : State machine file for the BLMAll class
//
// project :     BLMAll
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <BLMAll.h>

/*----- PROTECTED REGION END -----*/	//	BLMAll::BLMAllStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace BLMAll_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_TriggerCounter_allowed()
 *	Description : Execution allowed for TriggerCounter attribute
 */
//--------------------------------------------------------
bool BLMAll::is_TriggerCounter_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for TriggerCounter attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::TriggerCounterStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::TriggerCounterStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_AutoCalibration_allowed()
 *	Description : Execution allowed for AutoCalibration attribute
 */
//--------------------------------------------------------
bool BLMAll::is_AutoCalibration_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for AutoCalibration attribute in Write access.
	/*----- PROTECTED REGION ID(BLMAll::AutoCalibrationStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::AutoCalibrationStateAllowed_WRITE

	//	Not any excluded states for AutoCalibration attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::AutoCalibrationStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::AutoCalibrationStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_InjectionInProgress_allowed()
 *	Description : Execution allowed for InjectionInProgress attribute
 */
//--------------------------------------------------------
bool BLMAll::is_InjectionInProgress_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for InjectionInProgress attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::InjectionInProgressStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::InjectionInProgressStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_TotalLoss_allowed()
 *	Description : Execution allowed for TotalLoss attribute
 */
//--------------------------------------------------------
bool BLMAll::is_TotalLoss_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for TotalLoss attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::TotalLossStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::TotalLossStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_TotalInjectionLoss_allowed()
 *	Description : Execution allowed for TotalInjectionLoss attribute
 */
//--------------------------------------------------------
bool BLMAll::is_TotalInjectionLoss_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for TotalInjectionLoss attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::TotalInjectionLossStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::TotalInjectionLossStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_InjAvgLenght_allowed()
 *	Description : Execution allowed for InjAvgLenght attribute
 */
//--------------------------------------------------------
bool BLMAll::is_InjAvgLenght_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for InjAvgLenght attribute in Write access.
	/*----- PROTECTED REGION ID(BLMAll::InjAvgLenghtStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::InjAvgLenghtStateAllowed_WRITE

	//	Not any excluded states for InjAvgLenght attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::InjAvgLenghtStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::InjAvgLenghtStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_ZeroGainNoCurrent_allowed()
 *	Description : Execution allowed for ZeroGainNoCurrent attribute
 */
//--------------------------------------------------------
bool BLMAll::is_ZeroGainNoCurrent_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for ZeroGainNoCurrent attribute in Write access.
	/*----- PROTECTED REGION ID(BLMAll::ZeroGainNoCurrentStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ZeroGainNoCurrentStateAllowed_WRITE

	//	Not any excluded states for ZeroGainNoCurrent attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::ZeroGainNoCurrentStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ZeroGainNoCurrentStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_Mode_allowed()
 *	Description : Execution allowed for Mode attribute
 */
//--------------------------------------------------------
bool BLMAll::is_Mode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for Mode attribute in Write access.
	/*----- PROTECTED REGION ID(BLMAll::ModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ModeStateAllowed_WRITE

	//	Not any excluded states for Mode attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::ModeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ModeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_BLMNumber_allowed()
 *	Description : Execution allowed for BLMNumber attribute
 */
//--------------------------------------------------------
bool BLMAll::is_BLMNumber_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for BLMNumber attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::BLMNumberStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::BLMNumberStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_TimeResolvedInjLoss_allowed()
 *	Description : Execution allowed for TimeResolvedInjLoss attribute
 */
//--------------------------------------------------------
bool BLMAll::is_TimeResolvedInjLoss_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for TimeResolvedInjLoss attribute in read access.
	/*----- PROTECTED REGION ID(BLMAll::TimeResolvedInjLossStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::TimeResolvedInjLossStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_ResetError_allowed()
 *	Description : Execution allowed for ResetError attribute
 */
//--------------------------------------------------------
bool BLMAll::is_ResetError_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ResetError command.
	/*----- PROTECTED REGION ID(BLMAll::ResetErrorStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ResetErrorStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_ResetTriggerCounter_allowed()
 *	Description : Execution allowed for ResetTriggerCounter attribute
 */
//--------------------------------------------------------
bool BLMAll::is_ResetTriggerCounter_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ResetTriggerCounter command.
	/*----- PROTECTED REGION ID(BLMAll::ResetTriggerCounterStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ResetTriggerCounterStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_StartInjection_allowed()
 *	Description : Execution allowed for StartInjection attribute
 */
//--------------------------------------------------------
bool BLMAll::is_StartInjection_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for StartInjection command.
	/*----- PROTECTED REGION ID(BLMAll::StartInjectionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::StartInjectionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_StopInjection_allowed()
 *	Description : Execution allowed for StopInjection attribute
 */
//--------------------------------------------------------
bool BLMAll::is_StopInjection_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for StopInjection command.
	/*----- PROTECTED REGION ID(BLMAll::StopInjectionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::StopInjectionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_ForceStartInjection_allowed()
 *	Description : Execution allowed for ForceStartInjection attribute
 */
//--------------------------------------------------------
bool BLMAll::is_ForceStartInjection_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ForceStartInjection command.
	/*----- PROTECTED REGION ID(BLMAll::ForceStartInjectionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ForceStartInjectionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : BLMAll::is_ForceStopInjection_allowed()
 *	Description : Execution allowed for ForceStopInjection attribute
 */
//--------------------------------------------------------
bool BLMAll::is_ForceStopInjection_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ForceStopInjection command.
	/*----- PROTECTED REGION ID(BLMAll::ForceStopInjectionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	BLMAll::ForceStopInjectionStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(BLMAll::BLMAllStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	BLMAll::BLMAllStateAllowed.AdditionalMethods

}	//	End of namespace
