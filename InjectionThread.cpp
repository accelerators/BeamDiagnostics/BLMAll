//+=============================================================================
//
// file :         InjectionThread.cpp
//
// description :  Include for the InjectionThread class.
//                This class is used for BLMAll archiving during injection
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace BLMAll_ns
{
class InjectionThread;
}

#include <InjectionThread.h>

namespace BLMAll_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
InjectionThread::InjectionThread(BLMAll *blmall, omni_mutex &m,int cmd):
        Tango::LogAdapter(blmall), mutex(m), ds(blmall), command(cmd)
{

  INFO_STREAM << "InjectionThread::InjectionThread(): entering." << endl;

  tickStart = -1;
  exitThread = false;
  status = string("");
  lastError = string("");

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *InjectionThread::run_undetached(void *arg) {

  switch (command) {

    case LOOP_CMD:

      // Automatic detection of injection mode

      while (!exitThread) {

        {
          omni_mutex_lock lock(mutex);
          status = string("InjectionThread: AutoDetection is ON");
          if(lastError.length()>0)
            status += "\n"+string("InjectionThread: ") + lastError;
        }

        time_t t0 = get_ticks();

        int newMode = getMode();

        if (newMode != UNKNOWN_MODE && newMode != ds->injectionMode) {

          ds->injectionMode = CHANGING_MODE;

          if (newMode == INJECTION_MODE) {
            //cout << "Switching to Injection mode: " << getDate() << endl;
            setInjectionMode();
          } else {
            //cout << "Switching to normal mode: " << getDate() << endl;
            setNormalMode();
          }

          ds->injectionMode = newMode;

        }

        time_t t1 = get_ticks();

        time_t toSleep = (int) (500 - (t1 - t0));
        if (toSleep > 0) usleep((unsigned) (toSleep * 1000));

      }
      break;

    case SET_NORMAL_MODE_CMD:
      ds->injectionMode = CHANGING_MODE;
      setNormalMode();
      ds->injectionMode = NORMAL_MODE;
      break;

    case SET_INJ_MODE_CMD:
      ds->injectionMode = CHANGING_MODE;
      setInjectionMode();
      ds->injectionMode = INJECTION_MODE;
      break;

    case SET_FORCED_INJ_MODE_CMD:
      ds->injectionMode = CHANGING_MODE;
      setInjectionMode();
      ds->injectionMode = FORCED_INJECTION_MODE;
      break;

  }

}


// ----------------------------------------------------------------------------------------
// Return date
// ----------------------------------------------------------------------------------------
std::string InjectionThread::getDate() {

  time_t now = time(NULL);
  char *dateStr = ctime(&now);
  char tmp[128];
  tmp[0] = dateStr[8];
  tmp[1] = dateStr[9];
  tmp[2] = '/';
  tmp[3] = dateStr[4];
  tmp[4] = dateStr[5];
  tmp[5] = dateStr[6];
  tmp[6] = '/';
  tmp[7] = dateStr[20];
  tmp[8] = dateStr[21];
  tmp[9] = dateStr[22];
  tmp[10] = dateStr[23];
  tmp[11] = ' ';
  tmp[12] = dateStr[11];
  tmp[13] = dateStr[12];
  tmp[14] = dateStr[13];
  tmp[15] = dateStr[14];
  tmp[16] = dateStr[15];
  tmp[17] = dateStr[16];
  tmp[18] = dateStr[17];
  tmp[19] = dateStr[18];
  tmp[20] = 0;
  return string(tmp);

}

// ----------------------------------------------------------------------------------------
// Is injection
// ----------------------------------------------------------------------------------------
int InjectionThread::getMode() {

  Tango::DevState septumState;
  try {
    ds->septumDS->read_attribute("State") >> septumState;
  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    lastError = string("Cannot read septum device:\n") + e.errors[0].desc.in();
    return UNKNOWN_MODE;
  }

  return (septumState==Tango::ON || septumState==Tango::ALARM || septumState==Tango::MOVING)?INJECTION_MODE:NORMAL_MODE;

}

//----------------------------------------------------------------------------
// Write attribute
//----------------------------------------------------------------------------
template<typename inType>
void InjectionThread::write(string attName,inType value) {

  try {
    Tango::DeviceAttribute da(attName, value);
    ds->self->write_attribute(da);
  } catch (Tango::DevFailed &e) {}

}

//----------------------------------------------------------------------------
// Set normal mode
//----------------------------------------------------------------------------
void InjectionThread::setNormalMode() {


  //cout << "Complete frame  : " << ds->nbComplete << endl;
  //cout << "Incomplete frame: " << ds->nbIncomplete << endl;

  Tango::DeviceData argin;

  write("AvgEnable", (Tango::DevBoolean) false);
  write("AcqTrigSource", (Tango::DevULong) 0); // Off
  write("T2TrigSource", (Tango::DevLong64) 0); // Off

  write("AttenuationA", (Tango::DevULong) 20);
  write("AttenuationB", (Tango::DevULong) 20);
  write("AttenuationC", (Tango::DevULong) 20);
  write("AttenuationD", (Tango::DevULong) 20);

  write("TerminationA", (Tango::DevULong) 0);
  write("TerminationB", (Tango::DevULong) 0);
  write("TerminationC", (Tango::DevULong) 0);
  write("TerminationD", (Tango::DevULong) 0);

  write("CalibEnable", (Tango::DevBoolean) true);
  write("AbsEnable", (Tango::DevBoolean) true);

  // Reapply last configuration files
  try {
    for(size_t i=0;i<ds->settingDS.size();i++) {
      //cout << "Load " << ds->currentConfFiles[i] << endl;
      argin << ds->currentConfFiles[i];
      ds->settingDS[i]->command_inout("ApplySettings", argin);
    }
  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    lastError = string("Failed to switch to normal mode:\n") + e.errors[0].desc.in();
  }

  // Wait a bit that gains are applied before starting normal acquisition
  usleep(500000);

  write("SAEnable", (Tango::DevBoolean) true);
  usleep(500000);

}

//----------------------------------------------------------------------------
// Set normal mode
//----------------------------------------------------------------------------
void InjectionThread::setInjectionMode() {

  Tango::DeviceData argin;

  // Reset clock card
  try {
    ds->clockDS->command_inout("Stop");
    usleep(100000);
    ds->clockDS->command_inout("SyncContinuous");
  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    lastError = string("Failed to switch to injection mode:\n") + e.errors[0].desc.in();
  }

  // Sleep after the septum state change
  sleep(ds->injectionDelay);

  write("SAEnable", (Tango::DevBoolean) false);

  write("AcqTrigSource", (Tango::DevULong) 1); // External
  write("T2TrigSource", (Tango::DevLong64) 1); // External

  write("DecimationAvgN", (Tango::DevULong) 1);

  write("AttenuationA", (Tango::DevULong) 30);
  write("AttenuationB", (Tango::DevULong) 30);
  write("AttenuationC", (Tango::DevULong) 30);
  write("AttenuationD", (Tango::DevULong) 30);

  write("TerminationA", (Tango::DevULong) 1);
  write("TerminationB", (Tango::DevULong) 1);
  write("TerminationC", (Tango::DevULong) 1);
  write("TerminationD", (Tango::DevULong) 1);

  write("CalibEnable", (Tango::DevBoolean) true);
  write("AbsEnable", (Tango::DevBoolean) true);

  try {

    // Save lastAppliedFile
    ds->currentConfFiles.clear();
    for(size_t i=0;i<ds->settingDS.size();i++) {
      string file;
      ds->settingDS[i]->read_attribute("LastAppliedFile") >> file;
      ds->currentConfFiles.push_back(file);
    }

    for(size_t i=0;i<ds->settingDS.size();i++) {
      //cout << "Load " << ds->injectionConfFiles[i] << endl;
      argin << ds->injectionConfFiles[i];
      ds->settingDS[i]->command_inout("ApplySettings", argin);
    }

  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    lastError = string("Failed to switch to injection mode:\n") + e.errors[0].desc.in();
  }

  usleep(500000);

  write("AvgLength", (Tango::DevULong) ds->attr_InjAvgLenght_read[0]);
  write("AvgEnable", (Tango::DevBoolean) true);

}

//----------------------------------------------------------------------------
// Return number of milliseconds
//-----------------------------------------------------------------------------
time_t InjectionThread::get_ticks() {

  if(tickStart < 0 )
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}

} // namespace BLMAll_ns
