//+=============================================================================
//
// file :         ArchiveThreadTR.h
//
// description :  Include for the ArchiveThreadTR class.
//                This class is used for BLMAll archiving thread
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef BLMALL_ARCHIVETHREADTR_H
#define BLMALL_ARCHIVETHREADTR_H

#define MAX_FRAME_TIME      50       // Maximum time to recevive all events from BLM
#define MAX_TR_BUFF_LENGTH  4096     // Maximum size of Avg buffer length for Time Resolved loss

#include <BLMAll.h>

namespace BLMAll_ns {

class ArchiveThreadTR : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    ArchiveThreadTR(BLMAll *, omni_mutex &);
    void *run_undetached(void *);
    void event(int blmId,Tango::EventData *event);

    bool exitThread;
    string lastError;
    string status;

private:

    BLMAll *ds;
    omni_mutex &mutex;
    time_t tickStart;
    bool subscribed[MAX_BLM];
    bool lastErrors[MAX_BLM];
    time_t lastEvent[MAX_BLM];
    time_t frameStart;
    omni_mutex eventMutex;
    double frame[MAX_TR_BUFF_LENGTH];
    size_t length;
    int nbComplete;
    int nbIncomplete;

    time_t get_ticks();
    bool checkFrame();
    void resetFrame();
    void pushFrame();


}; // class ArchiveThreadTR

} // namespace BLMAll_ns

#endif //BLMALL_ARCHIVETHREADTR_H
