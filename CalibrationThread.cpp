//+=============================================================================
//
// file :         CalibrationThread.cpp
//
// description :  Include for the CalibrationThread class.
//                This class is used for BLM calibration process
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace BLMAll_ns
{
class CalibrationThread;
}

#include <CalibrationThread.h>
#include <math.h>

namespace BLMAll_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
CalibrationThread::CalibrationThread(BLMAll *blmall, omni_mutex &m):
        Tango::LogAdapter(blmall), mutex(m), ds(blmall)
{

  INFO_STREAM << "CalibrationThread::CalibrationThread(): entering." << endl;

  tickStart = -1;
  exitThread = false;
  nbStep = 0;
  status = string("");
  lastError = string("");

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *CalibrationThread::run_undetached(void *arg) {

  // Wait that server starts
  sleep(10);

  while (!exitThread) {

    time_t t0 = get_ticks();

    {
      omni_mutex_lock lock(mutex);
      status = string("CalibrationThread: Step: ") + std::to_string(nbStep);
      if(lastError.length()>0)
        status += "\n"+string("CalibrationThread: ") + lastError;
    }

    if( ds->attr_AutoCalibration_read[0] && ds->injectionMode==NORMAL_MODE ) {


      try {

        bool vgcChange = false;
        bool attChange = false;

        vector<double> vgc = readAtt("VGC");
        vector<double> att = readAtt("Attenuations");
        vector<double> losses = readAtt("Losses");
        vector<double> calibena = readAtt("CalibEnables");

        for(int i=0;i<ds->nbBLM*ds->nbChannel;i++) {

          if( valid(vgc[i],att[i],losses[i],calibena[i]) ) {


            if( equal(calibena[i/ds->nbChannel],1.0) ) {

              // Calibration enabled on Spark

              // Compute index
              int attIdx = -1;
              int vgcIdx = -1;

              if (equal(att[i], 0.0))
                attIdx = 0;
              else if (equal(att[i], 10.0))
                attIdx = 1;
              else if (equal(att[i], 20.0))
                attIdx = 2;
              else if (equal(att[i], 30.0))
                attIdx = 3;
              else {
                // Unexpected attenuation value
                omni_mutex_lock lock(mutex);
                lastError = "AutoCalibration failed : Unexpected attenuation value for " + name(i);
                continue;
              }

              if (equal(vgc[i], 0.0))
                vgcIdx = 0;
              else if (equal(vgc[i], 0.3))
                vgcIdx = 1;
              else if (equal(vgc[i], 0.4))
                vgcIdx = 2;
              else if (equal(vgc[i], 0.5))
                vgcIdx = 3;
              else if (equal(vgc[i], 0.6))
                vgcIdx = 4;
              else if (equal(vgc[i], 0.7))
                vgcIdx = 5;
              else if (equal(vgc[i], 0.8))
                vgcIdx = 6;
              else if (equal(vgc[i], 0.9))
                vgcIdx = 7;
              else {
                // Unexpected vgc value
                omni_mutex_lock lock(mutex);
                lastError = "AutoCalibration failed : Unexpected VGC value for " + name(i);
                continue;
              }


              double V = ds->calibrationLookup[i][vgcIdx * 4 + attIdx];
              double V04 = ds->calibrationLookup[i][2 * 4 + attIdx];
              double V05 = ds->calibrationLookup[i][3 * 4 + attIdx];
              double V06 = ds->calibrationLookup[i][4 * 4 + attIdx];
#ifdef PRINTCABLIB
              cout << "BLM#" << i << " Lookup:" << V << " att:" << att[i] << " vgc:" << vgc[i] << " Losses:" << losses[i] << endl;
#endif
              if (losses[i] > V) {

                if (vgc[i] >= 0.49) {
                  vgc[i] = vgc[i] - 0.1;
#ifdef PRINTCABLIB
                  cout << "BLM#" << i << " VGC change:" << vgc[i] << endl;
#endif
                  vgcChange = true;
                } else if (att[i] <= 20.01) {
                  att[i] = att[i] + 10.0;
#ifdef PRINTCABLIB
                  cout << "BLM#" << i << " Att change:" << att[i] << endl;
#endif
                  attChange = true;
                }

              } else if ( (losses[i] < V04/3.0) && ( att[i]==30.0 || equal(vgc[i], 0.4) ) ) {

                if( losses[i]<V06/2.0 ) {
                  att[i] = 20.0;
                  vgc[i] = 0.6;
#ifdef PRINTCABLIB
                  cout << "BLM#" << i << " VGC change:" << vgc[i] << endl;
                  cout << "BLM#" << i << " Att change:" << att[i] << endl;
#endif
                  vgcChange = true;
                  attChange = true;
                } else if( losses[i]<V05/2 ) {
                  att[i] = 20.0;
                  vgc[i] = 0.5;
#ifdef PRINTCABLIB
                  cout << "BLM#" << i << " VGC change:" << vgc[i] << endl;
                  cout << "BLM#" << i << " Att change:" << att[i] << endl;
#endif
                  vgcChange = true;
                  attChange = true;
                }

              }

            } else {

              // Calibration disabled on Spark
              if( losses[i] > 1455872 ) {
                if (vgc[i] >= 0.49) {
                  vgc[i] = vgc[i] - 0.1;
#ifdef PRINTCABLIB
                  cout << "(local calib diabled) BLM#" << i << " VGC change:" << vgc[i] << endl;
#endif
                  vgcChange = true;
                } else if (att[i] <= 20.01) {
                  att[i] = att[i] + 10.0;
#ifdef PRINTCABLIB
                  cout << "(local calib diabled) BLM#" << i << " Att change:" << att[i] << endl;
#endif
                  attChange = true;
                }
              } else if ( (losses[i] < 1455872.0/3.0 ) && (att[i]==30.0 || equal(vgc[i], 0.4)) ) {
                att[i] = 20.0;
                vgc[i] = 0.6;
#ifdef PRINTCABLIB
                cout << "(local calib diabled) BLM#" << i << " VGC change:" << vgc[i] << endl;
                cout << "(local calib diabled) BLM#" << i << " Att change:" << att[i] << endl;
#endif
                vgcChange = true;
                attChange = true;
              }

            }

          }
        }

        if( attChange ) {
          Tango::DeviceAttribute da("Attenuations",att);
          ds->self->write_attribute(da);
        }

        if( vgcChange ) {
          Tango::DeviceAttribute da("VGC",vgc);
          ds->self->write_attribute(da);
        }

      } catch (Tango::DevFailed &e) {

        omni_mutex_lock lock(mutex);
        lastError = string("AutoCalibration failed :\n") + e.errors[0].desc.in();

      }
      nbStep++;

    }

    time_t t1 = get_ticks();


    time_t toSleep = (int) (ds->calibrationPeriod*1000 - (t1 - t0));
    if (toSleep > 0) usleep((unsigned) (toSleep * 1000));

  }

  cout << "CalibrationThread exiting..." << endl;

}

//----------------------------------------------------------------------------
// Return name of a BLD
//----------------------------------------------------------------------------
string CalibrationThread::name(int idx) {

  char tmp[256];
  char format[256];

  if( ds->nbBLM==32 && ds->nbChannel==4 ) {
    int cell = (idx / 4) + 1;
    int bld = idx % 4;
    sprintf(tmp, "C%d-%s", cell, BLDNAMES[bld]);
    return string(tmp);
  } else {
    int cell = (idx / ds->nbChannel) + 1;
    int bld = idx % ds->nbChannel;
    sprintf(format,"%s%%d-%%s",ds->calibrationFilePrefix.c_str());
    sprintf(tmp, format, cell, BLDNAMES[bld]);
    return string(tmp);
  }

}

//----------------------------------------------------------------------------
// Read att
//----------------------------------------------------------------------------
vector<double> CalibrationThread::readAtt(string attName) {

  vector<double> ret;
  Tango::DeviceAttribute da;
  da = ds->self->read_attribute(attName);
  da >> ret;
  ret.resize(da.get_nb_read());
  return ret;

}

//----------------------------------------------------------------------------
// Return ok if given double are not NaN
//----------------------------------------------------------------------------
bool CalibrationThread::valid(double &g,double &a,double &l,double &c) {

  return !__isnan(g) && !__isnan(a) && !__isnan(l) && !__isnan(c);

}

//----------------------------------------------------------------------------
// Compare 2 double
//----------------------------------------------------------------------------
bool CalibrationThread::equal(double d1,double d2) {

  return fabs(d1-d2)<1e-2;

}

//----------------------------------------------------------------------------
// Return number of milliseconds
//-----------------------------------------------------------------------------
time_t CalibrationThread::get_ticks() {

  if(tickStart < 0 )
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}

} // namespace BLMAll_ns
