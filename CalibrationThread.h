//+=============================================================================
//
// file :         CalibrationThread.h
//
// description :  Include for the CalibrationThread class.
//                This class is used for BLM calibration process
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef BLMALL_CALIBRATIONTHREAD_H
#define BLMALL_CALIBRATIONTHREAD_H

#include <BLMAll.h>

namespace BLMAll_ns {

class CalibrationThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    CalibrationThread(BLMAll *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string lastError;
    string status;

private:

    BLMAll *ds;
    omni_mutex &mutex;
    time_t tickStart;
    int nbStep;

    vector<double> readAtt(string attName);
    bool equal(double d1,double d2);
    bool valid(double &g,double &a,double &l,double &c);
    string name(int idx);
    time_t get_ticks();


}; // class CalibrationThread

} // namespace BLMAll_ns

#endif //BLMALL_CALIBRATIONTHREAD_H
