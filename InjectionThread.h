//+=============================================================================
//
// file :         InjectionThread.h
//
// description :  Include for the InjectionThread class.
//                This class is used for BLMAll archiving during injection
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef BLMALL_INJECTIONTHREAD_H
#define BLMALL_INJECTIONTHREAD_H

#include <BLMAll.h>

#define UNKNOWN_MODE 0
#define NORMAL_MODE 1
#define INJECTION_MODE 2
#define FORCED_INJECTION_MODE 3
#define CHANGING_MODE 4

#define LOOP_CMD 1
#define SET_NORMAL_MODE_CMD 2
#define SET_INJ_MODE_CMD 3
#define SET_FORCED_INJ_MODE_CMD 4

namespace BLMAll_ns {

class InjectionThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    InjectionThread(BLMAll *, omni_mutex &, int cmd);
    void *run_undetached(void *);
    bool exitThread;
    int command;
    string lastError;
    string status;

private:

    void setNormalMode();
    void setInjectionMode();
    int  getMode();

    template<typename inType>
    void write(string attName,inType value);
    std::string getDate();

    BLMAll *ds;
    omni_mutex &mutex;
    time_t tickStart;

    time_t get_ticks();


}; // class InjectionThread

} // namespace BLMAll_ns

#endif //BLMALL_INJECTIONTHREAD_H
