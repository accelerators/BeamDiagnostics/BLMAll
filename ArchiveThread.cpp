//+=============================================================================
//
// file :         ArchiveThread.cpp
//
// description :  Include for the ArchiveThread class.
//                This class is used for BLMAll archiving thread
//
// project :      BLMAll TANGO Device Server
//
// $Author: pons$
//
// $Revision: $
//
// $HeadURL: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace BLMAll_ns
{
class ArchiveThread;
}

#include <ArchiveThread.h>

static double NaN = sqrt(-1);

namespace BLMAll_ns
{


// ----------------------------------------------------------------------------------------
// Inner class for Tango event callback
// ----------------------------------------------------------------------------------------
class CallBacks: public Tango::CallBack {

public:
    CallBacks() {}

    void Init(int blmId,ArchiveThread *src) {
      id = blmId;
      source = src;
    }

    void push_event(Tango::EventData *event) {
      source->event(id,event);
    }

    int id;
    ArchiveThread *source;

};

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
ArchiveThread::ArchiveThread(BLMAll *blmall, omni_mutex &m):
        Tango::LogAdapter(blmall), mutex(m), ds(blmall)
{

  INFO_STREAM << "ArchiveThread::ArchiveThread(): entering." << endl;

  tickStart = -1;
  for(int i=0;i<MAX_BLM;i++) {
    subscribed[i] = false;
    lastErrors[i] = true;
    lastEvent[i] = 0;
  }
  exitThread = false;
  frameStart = 0;
  status = string("");
  lastError = string("");
  nbComplete = 0;
  nbIncomplete = 0;

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ArchiveThread::run_undetached(void *arg) {

  CallBacks callBacks[MAX_BLM];
  for(int i=0;i<MAX_BLM;i++)
    callBacks[i].Init(i,this);

  while (!exitThread) {

    time_t t0 = get_ticks();

    {
      omni_mutex_lock lock(mutex);
      status = string("ArchiveThread: Got frames (complete/incomplete): ") + std::to_string(nbComplete) + "/" + std::to_string(nbIncomplete);
      if(lastError.length()>0)
        status += "\n"+string("ArchiveThread: ") + lastError;
    }

    // Subscribe to all BLMs
    for(int i=0;i<ds->nbBLM;i++) {
      if (!subscribed[i]) {
        try {
          const vector<string> filters;
          Tango::DeviceProxy *dp = ds->deviceGroup->get_device(i+1); // get device starts at one !!!!!
          if( dp!=NULL ) {
            int subscriber_id = dp->subscribe_event("AvgSum", Tango::CHANGE_EVENT, &callBacks[i], filters);
            subscribed[i] = true;
          }
        } catch (Tango::DevFailed &e) {
          //cout << "Warning,  " << ds->blmGroup->get_device(i)->dev_name() << "/AvgSum " << e.errors[0].desc << endl;
        }
      }
    }

    // Check frame
    time_t now = get_ticks();
    if( frameStart!=0 && (now-frameStart) > MAX_FRAME_TIME ) {

      if(!checkFrame()) {

        // Imcomplete frame
        cout << "Imcomplete frame !" << endl;
        int nbError = 0;

        for(int i=0;i<ds->nbBLM;i++) {
          if(!ds->isDisabled(i) && subscribed[i] && lastEvent[i]==0) {
            cout << "BLM C" << (i+1) << " : no data " << endl;
            nbError++;
          }
        }

        if(nbError<5) {
          // Store if less than 5 errors
          nbIncomplete++;
          pushFrame();
        }
        resetFrame();

      }

    }


    time_t t1 = get_ticks();


    time_t toSleep = (int) (100 - (t1 - t0));
    if (toSleep > 0) usleep((unsigned) (toSleep * 1000));

  }

  cout << "ArchiveThread exiting..." << endl;

}

//----------------------------------------------------------------------------
// Check if a frame is complete
//----------------------------------------------------------------------------
bool ArchiveThread::checkFrame() {

  // Check that all events have been sent
  bool ok = true;
  int i = 0;
  while (i < ds->nbBLM && ok) {
    // Ignore when not subscribed or disabled
    ok = ds->isDisabled(i) || !subscribed[i] || lastEvent[i] != 0;
    i++;
  }

  return ok;

}

//----------------------------------------------------------------------------
// Push frame to HDB++
//----------------------------------------------------------------------------
void ArchiveThread::pushFrame() {

  try {

    double *frameCopy = new double[ds->nbBLM*4];
    memcpy(frameCopy,frame,ds->nbBLM*4*sizeof(double));
    // Store in HDB only if we are in AutoDetection mode
    // if( ds->attr_AutoDetectInjection_read[0] )
      ds->push_archive_event("InjLosses", frameCopy, ds->nbBLM * 4, 0, true);

    // Total injection loss
    double *sum = new double[1];
    sum[0] = 0;
    for(int i=0;i<ds->nbBLM*4;i++)
      if(!std::isnan(frameCopy[i])) sum[0] += frame[i];
    ds->attr_TotalInjectionLoss_read[0] = sum[0];
    // Store in HDB only if we are in AutoDetection mode
    // if( ds->attr_AutoDetectInjection_read[0] )
      ds->push_archive_event("TotalInjectionLoss",sum,1,0,true);

  } catch (Tango::DevFailed &e) {
    omni_mutex_lock lock(mutex);
    lastError = string("Failed to push data to HDB :") + e.errors[0].desc.in();
  }

}

//----------------------------------------------------------------------------
// Reset frame
//----------------------------------------------------------------------------
void ArchiveThread::resetFrame() {

  frameStart = 0;
  for (int i = 0; i < ds->nbBLM; i++) {
    lastEvent[i] = 0;
    frame[i*4+0] = NaN;
    frame[i*4+1] = NaN;
    frame[i*4+2] = NaN;
    frame[i*4+3] = NaN;
  }

}

//----------------------------------------------------------------------------
// Event callback
//----------------------------------------------------------------------------
void ArchiveThread::event(int blmId, Tango::EventData *event) {

  // Serialise call
  omni_mutex_lock lock(eventMutex);

  // Ignore subscription event and event coming from synchronous reading
  if(lastErrors[blmId]) {
    lastErrors[blmId]=0;
    return;
  }

  time_t now = get_ticks();

  if (!event->err) {

    try {

      vector<Tango::DevLong> data;
      *(event->attr_value) >> data;
      if(data.size()!=4) {
        cout << "ArchiveThread::event() got error for BLM#" << blmId << " : Invalid data size !" << endl;
        return;
      }

      if (frameStart==0) {
        // We start a new frame
        // cout << "Starting new frame " << endl;
        resetFrame();
        frameStart = now;
      }

      for(int i=0;i<(int)data.size();i++)
        frame[blmId*4+i] = (double)data[i];
      lastEvent[blmId] = now;

      if (checkFrame()) {
        // Complete frame
        // cout << "Complete frame:" << (now - frameStart) << " ms" << endl;
        nbComplete++;
        pushFrame();
        resetFrame();
      }

    } catch (Tango::DevFailed &e) {

      cout << "ArchiveThread::event() got error for BLM#" << blmId << " " << e.errors[0].desc << endl;

    }

  } else {

    lastErrors[blmId] = true;
    cout << "ArchiveThread::event() got error for BLM#" << blmId << " " << event->errors[0].desc << endl;

  }


}

//----------------------------------------------------------------------------
// Return number of milliseconds
//-----------------------------------------------------------------------------
time_t ArchiveThread::get_ticks() {

  if(tickStart < 0 )
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}

} // namespace BLMAll_ns
